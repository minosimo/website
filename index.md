---
layout: home
---

Hi, I am Schmiddi. I am currently studying computer science at the University of Passau.

# What I am Working on

Currently, my main projects are:

- [Flare](https://gitlab.com/schmiddi-on-mobile/flare): Chat with your friends on Signal
- [Pipeline](https://gitlab.com/schmiddi-on-mobile/pipeline): Watch YouTube, and PeerTube videos in one place
- [Railway](https://gitlab.com/schmiddi-on-mobile/railway): Travel with all your train information in one place

For a list of all my public projects and projects I am contributing to, see my [GitLab](https://gitlab.com/users/Schmiddiii/projects) (including the [Schmiddi on Mobile Group](https://gitlab.com/schmiddi-on-mobile)) and [GitHub](https://github.com/Schmiddiii?tab=repositories) profile respectively.

# My Goals

As you can see from the projects I am working on, I am trying to improve the user-facing experience of Linux smartphones like the [PinePhone](https://www.pine64.org/pinephone/). In my process of writing applications that I wanted on my PinePhone, I personally enhanced my experience of the device to "that's cool" to "mostly daily-drivable". As from feedback I got from these applications, I am not the only one having such benefits from my work. My future shorter-term goal is to further enhance my current applications and maybe create new ones such that many more users can enjoy their mobile Linux experience.

For further information on this goal, see also <https://mobile.schmidhuberj.de/>, which represents an overview of my work regarding Linux mobile (note: this website is not primarily maintained by me, but I officially endorse it).

# Accounts

I am mostly active on GitLab and GitHub, although these days I prefer GitLab. Note that I am not on Facebook, Instagram, Twitter, Threads, Bluesky, TikTok, Snapchat, Pinterest, LinkedIn, Reddit, Myspace, YouTube, Google+, or any other popular social media. If you find any account over there, it is not me.

- [GitLab](https://gitlab.com/Schmiddiii)
- [GitHub](https://github.com/Schmiddiii)

Note that there is also the Mastodon account [@schmiddionmobile](https://fosstodon.org/@schmiddionmobile) for the Linux mobile applications. This is not maintained by me, but officially endorsed.

# How to Reach Me

Feel free to write me an E-Mail to any of my public E-Mail addresses visible on GitLab or GitHub. You can also write me on [the Matrix](https://matrix.org/) to [@schmiddi:matrix.org](https://matrix.to/#/@schmiddi:matrix.org).
