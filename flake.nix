{
  description = "My personal website";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        {
        packages = {
          default = 
            with import nixpkgs { inherit system; }; 
            stdenv.mkDerivation {
              buildInputs = with pkgs; [bundler jekyll rubyPackages.jekyll-feed ];
              name = "website";
              src = ./.;
              installPhase = ''
                mkdir -p $out/bin/
                cp run.sh $out/bin/
                chmod +x $out/bin/run.sh
              '';
            };
          publish = 
            with import nixpkgs { inherit system; }; 
            stdenv.mkDerivation {
              buildInputs = with pkgs; [bundler jekyll rubyPackages.jekyll-feed ];
              name = "build";
              src = ./.;
              installPhase = ''
                mkdir $out
                jekyll build -d $out/public
              '';
            };
        };

        devShells.default = 
            with import nixpkgs { inherit system; }; 
            stdenv.mkDerivation {
              buildInputs = with pkgs; [bundler jekyll  rubyPackages.jekyll-feed ];
              name = "website";
              src = ./.;
            };
            apps.default = {
              type = "app";
              program = "${self.packages.${system}.default}/bin/run.sh";
            };
        })
    );
}
