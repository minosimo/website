---
layout: post
title: Books, Games and Movies
tags: [ april-cools book game movie ]
---

This is an [April Cools](https://www.aprilcools.club/) post.
April Cools is about publishing content different to the usual content (in my case computer science).
I therefore decided to talk about books, games and movies.

I love books, games and movies (ordered in alphabetical order, not in preference order).
So I thought why not show off some of my favorites?

## Books

I don't read tons of books, maybe approximately one book per month, with an average of about 500 pages.
Pretty much all the books I read are (science-)fiction books.
I also don't read multiple books at once, I don't see how this enjoyable with the types of books I usually read.
I usually get my books as e-books from the local library, for which I have to pay yearly a minor amount of money.

From the books I have read in recent years, one author stands out very much.
[Cassandra Clare](https://cassandraclare.com/) is an excellent author I enjoy I have already read most books of.
Her books are fantastic, especially her [Shadowhunter Chronicles](https://cassandraclare.com/shadowhunters-timeline/).
The best trilogy of her is (so far at least, and of course in my opinion) "The Infernal Devices", the trilogy has by far the best ending of a series I have ever read.
I also liked her most recent book "Sword Catcher"; I even got this book as a hardcover, something I did not do for years.

Another author to point out is [Trudi Canavan](https://www.trudicanavan.com/) with her "The Black Magician" trilogy and the "Millennium's Rule" series, I also loved those books.
I should certainly also point out "Ready Player One" and "Ready Player Two" by [Ernest Cline](https://en.wikipedia.org/wiki/Ernest_Cline) (who also has a website, which just seems to forward to some metaverse project); two great books.

I also read "Project Hail Mary" by [Andy Weir](https://andyweirauthor.com/) (which has a terrible German title by the way), "Forward the Foundation" by [Isaac Asimov](https://en.wikipedia.org/wiki/Isaac_Asimov) (from which I plan to read a lot more books in the future) and started reading the Dune books by [Frank Herbert](https://en.wikipedia.org/wiki/Frank_Herbert), and have read many other books not mentioned in this short overview.

## Games

I also play games a lot (depending on your definition).
Depending on how much time I have, up to two hours a day (though during the semester, I only play on weekends).
How long I play a game very much depends on the game itself, but this can range from about 10 hours for the shorter games to 100--200 hours for longer ones or ones that I play regularly (and even longer for some games I will list below).
The games I play are mostly story-based games, but there are also others like puzzle games involved.
For most games I did not buy them full-prized; I either got them for free from the Epic Games Store or waited until a sale in Steam and got them there.
There were a few games though I got full-prized.

The first game I should mention is probably Minecraft, which I have played for many years when I was a child.
I think I probably have 1000--2000 hours played in Minecraft alone (I think I played it about 3 years exclusively).
It pretty much was the game I started off with (this may also have been Star Wars, Battlefront II (the original one); I don't quite remember).
I don't play Minecraft anymore though, apart from maybe checking out some modpack yearly.
I still watch quite a lot of Minecraft content though, e.g. Hermitcraft.

Another game I have played for probably a few hundred hours is "Star Wars: The Old Republic".
It is a pretty good game (though not great).
I played that pretty much every time I did not know what to play, which is why over the years I have managed to play through the main stories of all 8 character classes, granting me the "legendary player" status.

A similar game is probably "No Man's Sky", which I also sometimes play with a friend.
Even though I already "played through" the game (i.e. all story missions, and got the best of pretty much everything), it is still enjoyable to start the game and check out what was recently added.

You may have noticed that all previous games are not that story focused (at least I would not consider them to be).
That is because most story-focused games are pretty short and there is not much I can say about them except that I enjoy them.
But I still should mention a few.
One is certainly "Cyberpunk 2077", which I am in the process of playing through it a third time.
It's just a great story with good side quests.
Another one is certainly "The Witcher 3 - Wild Hunt" (and also "The Witcher 2 - Assassins of Kings") which has an outstanding story and gameplay, but which I find to long to play though again (for now); nevertheless this one is probably the best game I have ever played.
The Metro-series is certainly also something I should mention, all games were enjoyable, but I liked "Metro Exodus" the best.
Furthermore, BioShock is also a great series (except for the second one; I did not enjoy that one too much); I liked "BioShock Infinite" the most.

I also enjoyed the Portal games, "Portal", "Portal 2", "Portal Reloaded" (Modded) and "Portal Revolution" (Modded).

## Movies

Regarding movies, I again mostly watch fiction.
I either watch movies in the cinema with friends, if they are new, or on television (the old television, no streaming services).

Probably my most favorite movie is Interstellar.
It just got a great story, audio and visuals.
Even though I have seen that movie a few times now, I still like watching it on TV when it comes.

Another favorite is probably "Ready Player One", which is fun to watch.
As mentioned above, I also really liked the book it is based on; the story of the movie is a heavily compressed version of the story of the book.
I probably like the story of the book more, but nevertheless, the movie is also great.

Other movies to mention are "Pulp Fiction", the recent Dune movies, the "Guardians of the Galaxy" movies and also the X-Men movies.

In general, I also enjoy Marvel movies, but I feel like their quality (or something else) got drastically worse in recent years.
And they are starting to get extremely interconnected, to the point that some things can be confusing if you missed watching a movie or some series.

I also don't like that many recent movies are becoming two-part movies.
While I understand that many stories are too big to fit into one movie, having a big cliffhanger after the first movie to wait for multiple years until the second one (and in the meantime forgetting most things about the first) is not too good (e.g. the new "Spider-Man: A New Universe" movies or "Mission Impossible: Dead Reckoning").
There are also movies which do that better though, e.g. Dune had no real cliffhanger, one just was excited to see the second part; though my experience may also have been influenced by me reading the book after the first movie came out and therefore having an improved memory of the story.

## Conclusion

All three forms of entertainment are great in my opinion.
I could also talk about other great forms of entertainment, e.g. blogs or videos, but those three should suffice for now.
Any other forms of entertainments you enjoy?
Or any recommendations for books, games or movies I could enjoy?
Feel free to let me know.

Maybe I should review books, games and movies on this blog?
