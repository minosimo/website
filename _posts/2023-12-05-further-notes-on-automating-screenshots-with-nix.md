---
layout: post
title: Further notes on Automating Screenshots with Nix
tags: [ nix ]
---

This is a quick follow-up post to [my last post](/2023/11/30/automating-screenshots-with-nix.html) with three more notes I have and some refactorings. I learned about those improvements while porting my work to Railway.

The first thing to note is that your application needs to be DBus-activatable. I missed this with the Flare implementation as this was previously implemented to be DBus-activatable. Making it DBus-activatable is pretty trivial, just one new `*.service`-file, one minor addition to the desktop file and one addition to the corresponding `meson.build`.

Another important thing to note: Your application can only have an internet connection when the test is run in interactive mode. To be honest, I am very surprised that even in that case the VM gets internet access, but I guess it makes sense as the VM is outside of a Nix build in the interactive mode. This is very handy as I did not need to somehow supply dummy data to the application (like I did with Flare). As the GUI window in interactive mode is not really needed in that case, one can deactivate that with `virtualisation.graphics = false`.

The final note for today is that most of bug hunting I did with fixing the resolution of the (virtual) display was actually useless, the Flathub docs specify that the window should have an approximate size of `1000x700`, for which the default size suffices. The corresponding qemu-option can therefore be deleted.

And finally, the refactored version of the derivation:

```nix
# Note: This may only be run interactively if the application requires network access.
packages.makeScreenshot =
let
  nixos-lib = import (nixpkgs + "/nixos/lib") { };
in
nixos-lib.runTest {
  name = "screenshot";
  hostPkgs = pkgs;
  imports = [
    {
      nodes = {
        machine = { pkgs, ... }: {
          boot.loader.systemd-boot.enable = true;
          boot.loader.efi.canTouchEfiVariables = true;

          services.xserver.enable = true;
          services.xserver.displayManager.gdm.enable = true;
          services.xserver.desktopManager.gnome.enable = true;
          services.xserver.displayManager.autoLogin.enable = true;
          services.xserver.displayManager.autoLogin.user = "alice";

          users.users.alice = {
            isNormalUser = true;
            extraGroups = [ "wheel" ];
            uid = 1000;
          };

          system.stateVersion = "22.05";

          virtualisation.graphics = false;

          environment.systemPackages = [
            self.packages.${system}.default
          ];

          systemd.user.services = {
            "org.gnome.Shell@wayland" = {
              serviceConfig = {
                ExecStart = [
                  ""
                  "${pkgs.gnome.gnome-shell}/bin/gnome-shell"
                ];
              };
            };
          };
        };
      };

      testScript = { nodes, ... }:
        let
          lib = pkgs.lib;
          l = lib.lists;

          user = nodes.machine.users.users.alice;
          username = user.name;
          uid = toString user.uid;

          bus = "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/${uid}/bus";
          su = command: "su - ${user.name} -c '${command}'";

          type = word: "machine.send_chars(\"${word}\")";
          key = key: "machine.send_key(\"${key}\")";
          sleep = duration: "machine.sleep(${toString duration})";

          execution = [
            (type "Berlin Hbf")
            (sleep 2)
            (key "tab")
            (type "PARIS")
            (sleep 2)
            (l.replicate 5 (key "tab"))
            (key "ret")
            (sleep 5)
            (l.replicate 11 (key "tab"))
            (key "ret")
            (l.replicate 3 (key "tab"))
            (key "ret")
          ];


          preExecution = [
            (sleep 10)
            (key "esc")
            "machine.succeed(\"${launch}\")"
            (key "esc")
          ];

          postExecution = [
            (key "alt-print") # XXX: This for some reason sometimes fails. No idea why.
            "machine.execute(\"mv /home/${username}/Pictures/Screenshots/* screenshot.png\")"
            "machine.copy_from_vm(\"screenshot.png\", \".\")"
          ];

          fullExecution = l.flatten [preExecution (sleep 5) execution (sleep 5) postExecution];

          code = lib.concatStringsSep "\nmachine.sleep(1)\n" fullExecution;

          # Start Railway
          launch = su "${bus} gapplication launch de.schmidhuberj.DieBahn";
        in
          code;
    }
  ];
};
```

This code should now be very easily be transferable to any other project, the only changes which are needed is modification of `environment.systemPackages` to add your package, modification to `launch` to replace the application ID with your application, and finally modifications in `execution` specifying what to do before taking the screenshot (with nice macros for typing, key-pressing or sleeping).
# References

- [Railway](https://gitlab.com/schmiddi-on-mobile/railway), my public transport GTK application.
- [The Railway MR](https://gitlab.com/schmiddi-on-mobile/railway/-/merge_requests/89), which introduces the mentioned changes.
- [Flathub Doc](https://docs.flathub.org/docs/for-app-authors/appdata-guidelines/quality-guidelines/#reasonable-window-size), which specify the window size to be used in screenshots.
