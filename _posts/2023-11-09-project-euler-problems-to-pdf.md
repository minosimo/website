---
layout: post
title: Project Euler Problems to PDF
tags: [ programming ]
---

Recently, I thought about investigating and attempting some problems of Project Euler. But I did not really always want to go to the website to look at the questions, which is why I decided to convert their problems to PDF pages.

In theory, which also works for most of the problems in practice, the conversion is just throwing the (minimal) HTML into pandoc and generate PDFs out of it. This can easily be done in the command line:

```sh
curl https://projecteuler.net/minimal=1 | pandoc -f "html+tex_math_dollars+tex_math_single_backslash" -t latex -o problem1.pdf
```

Sadly, it gets more complicated than that. Firstly, I want to download multiple problems at once, and also save the title of of the problem in the PDF. Luckily, Project Euler has the `/minimal=problems`-endpoint for querying all the problems such that I can list their IDs and names and choose which to download. The thing which is not so easy to solve are the tons of invalid LaTeX code used in Project Euler (e.g. nested math environments, UTF-8 characters, some commands not available by default and some deprecated commands). To be honest, I still have not yet been able to successfully download every single problem, but I have created a Janet script that fixes most of what I have found. It can download the first 250 problems without any issues, afterwards there are some problems which I have not yet figured out how to solve some issues I had (e.g. deprecated `\\cases` command in `amsmath`). I have tested the script with about the first 500 problems (in 50-problem batches) and was able to download 400 of them (and would probably be able to download most of the 100 remaining ones, but due to the batching I did, already a single problem causes 50 problems to fail conversion).

Anyway, here is the Janet-code I wrote for this task. The first two variables specify which problems to download, the program will then first download the index and all the desired problems (with added headers for each problem), clean up the output and ask pandoc to convert it to HTML (with some nicer-looking options).

<!-- Note that this is a lisp-comment block, but the code itself is Janet. For some reason, the SSG produces a weird output when this is Janet -->
```lisp
#!/usr/bin/env janet

(var start 1)
(var limit 50)

(def [curl-list-r curl-list-w] (os/pipe))

(def [curl-html-r curl-html-w] (os/pipe))
(def [pandoc-html-r pandoc-html-w] (os/pipe))

(os/execute
  @("curl" "https://projecteuler.net/minimal=problems")
  :px
  {:out curl-list-w})
(:close curl-list-w)

(var lines (array/slice (string/split "\n" (:read curl-list-r :all)) (- start 1) (+ limit start)))
(array/remove lines 0)

(each line lines 
    (var cols (string/split "##" line))
    (var id (get cols 0))
    (var title (get cols 1))

    (if (not (nil? title))
        (:write curl-html-w (string/join ["<h1>" id ": " title "</h1>"]))
    )

    (os/sleep 1)

    (os/execute
      @("curl" (string/join ["https://projecteuler.net/minimal=" id]))
      :px
      {:out curl-html-w})
)

(:close curl-html-w)

# Replace weird things by correct latex.
(var text (:read curl-html-r :all))

(var text (string/replace-all "\\lt" "<" text))
(var text (string/replace-all "&lt;" "<" text))
(var text (string/replace-all "\\gt" ">" text))
(var text (string/replace-all "&gt;" ">" text))
(var text (string/replace-all "$$\\[" "\\[" text))
(var text (string/replace-all "\\]$$" "\\]" text))
(var text (string/replace-all "$\\[" "\\[" text))
(var text (string/replace-all "\\]$" "\\]" text))
(var text (string/replace-all "\\[$$" "\\[" text))
(var text (string/replace-all "$$\\]" "\\]" text))
(var text (string/replace-all "\\[$" "\\[" text))
(var text (string/replace-all "\\]$" "\\]" text))
(var text (string/replace-all "\\begin{array}" "$$\\begin{array}" text))
(var text (string/replace-all "\\end{array}" "\\end{array}$$" text))
(var text (string/replace-all "\\begin{matrix}" "$$\\begin{matrix}" text))
(var text (string/replace-all "\\end{matrix}" "\\end{matrix}$$" text))
(var text (string/replace-all "$$\\begin{align}" "\\begin{align}" text))
(var text (string/replace-all "\\end{align}$$" "\\end{align}" text))
(var text (string/replace-all "$\\begin{align}" "\\begin{align}" text))
(var text (string/replace-all "\\end{align}$" "\\end{align}" text))
(var text (string/replace-all "$$\\begin{align*}" "\\begin{align*}" text))
(var text (string/replace-all "\\end{align*}$$" "\\end{align*}" text))
(var text (string/replace-all "$\\begin{align*}" "\\begin{align*}" text))
(var text (string/replace-all "\\end{align*}$" "\\end{align*}" text))
(var text (string/replace-all "$$\n\\begin{align}" "\\begin{align}" text))
(var text (string/replace-all "\\end{align}\n$$" "\\end{align}" text))
(var text (string/replace-all "$\n\\begin{align}" "\\begin{align}" text))
(var text (string/replace-all "\\end{align}\n$" "\\end{align}" text))
(var text (string/replace-all "\\begin{align}" "\n\\begin{align}" text))
(var text (string/replace-all "$$\n\\begin{align*}" "\\begin{align*}" text))
(var text (string/replace-all "\\end{align*}\n$$" "\\end{align*}" text))
(var text (string/replace-all "$\n\\begin{align*}" "\\begin{align*}" text))
(var text (string/replace-all "\\end{align*}\n$" "\\end{align*}" text))
(var text (string/replace-all "\\begin{align*}" "\n\\begin{align*}" text))
(var text (string/replace-all "\\pu" "" text))
(var text (string/replace-all "\\style{font-family:inherit;}" "" text))
# Underlines require soul package, which I am currently unable to include for some reason.
(var text (string/replace-all "<u>" "" text))
(var text (string/replace-all "</u>" "" text))
# # Slightly different unicode characters
(var text (string/replace-all "−" "-" text))
(var text (string/replace-all "≥" " $\\geq$ " text))
(var text (string/replace-all "≤" " $\\leq$ " text))
(var text (string/replace-all " " " " text))

# TODO: 251-300 Requires soul package for underlining. Underlinings are currently ignored.
# TODO: 251-300 Requires deprecated \cases.
# TODO: 301-350 Requires deprecated \cases.

(:write pandoc-html-w text)
(:close pandoc-html-w)

# Maybe use wkhtmltopdf instead of latex?
(os/execute
  @("pandoc" "-" "-s" "--top-level-division" "chapter" "-f" "html+tex_math_dollars+tex_math_single_backslash" "-t" "latex" "-V" "pagestyle=empty" "-V" "geometry:margin=1in" "-V" "documentclass=report" "-o" "euler.pdf")
  :px
  {:in pandoc-html-r})
```

References:
- <https://projecteuler.net/archives>
- <https://pfischbeck.de/en/posts/projecteuler-api/>
- <https://pandoc.org/>
- <https://janet-lang.org/>
