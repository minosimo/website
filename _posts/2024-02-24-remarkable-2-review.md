---
layout: post
title: reMarkable 2 Review
subtitle: Paper replacement or just expensive paper-weight
tags: [ review, remarkable-2 ]
---

I got myself a reMarkable 2 quite some time ago.
And I have a few things to say about it, so why not write a post about it.

Note though that as always bad design issues are by far more noticeable compared to good design, therefore "Meh" and "Bad" sections are longer compared to the "good" section.
Nevertheless, I enjoy the reMarkable and will keep it, as noted in the conclusion.

## Usage

I use my reMarkable for mainly four things.
Firstly is a general TODO list as the first page of the quick sheet; this is mostly split up into study-related things and freetime-related things based on the position of the note and I will fully remove what I have written once done.
Furthermore, I use it to take notes about what I am currently working on and what needs to be done still; these notes go into a respective notebook for its purpose, with one day or topic for each page; those notes will also be kept for archival.
I also use it to review PDF documents from studying, e.g. scripts of slides.
Finally, I have also used it to create a summary of a lecture I took part in this semester.

I use my reMarkable 2 exclusively offline (except for software updates).
This of course prevents me from using their cloud service, but I see no reason why they should have my notes.
As you will note, this has a few drawbacks that will be discussed later on.

## The Good

There are quite a few things I like about the reMarkable 2.
The device itself feels great to use and high-quality.
I would certainly not say it is like writing on paper, but it still feels great to write on.
The overall design of the device, pen and OS is pretty simplistic, which is good in my eyes.

I especially like reviewing lecture material like scripts or slides on the reMarkable, it is just way more convenient compared to pulling out my laptop or using the tiny screen on my phone (which I rarely did).

They also have some pretty good documentation in form of their user-guide, which was very helpful to investigate its capabilities before buying the device.

## The Meh

I very much like the idea of the USB interface, plugging in your device via USB and easily transferring files between your computer and the device.
This also has a good API that can be used in programs (like I did for a script in Arc to transfer files from Arc to the reMarkable).
Nevertheless, I must say its still not great and has quite a few issues.
The most prominent issue is probably that this only allows exporting your reMarkable 2 documents as PDFs, this is therefore not really usable for backups of your notebooks to your computer.
A less important issue is that it only allows drag-and-drop upload in the browser, not even a button where one can select a files.
This is not a too big issue as I mostly interact with the interface using the API via my Arc-script.

Scrolling and zooming around documents does not feel fast, with some visible artifacts being left behind.
This is most likely a limitation of the e-ink display.
Its not too big of an issue in my eyes, the artifacts are really minor and the screen will refresh soon after and "fix" itself.

The device itself is also not too big, I would have expected something similar to Din A4, but it is considerably smaller.
This also has advantages though, like it being easier to hold with one hand.
Related to that, I find it weird that their provided marker sizes and templates are bigger than I expected, for me the small marker size looks normal while even the small grid for me is somewhat big.
Note that I am writing pretty small though, so maybe this is more suitable for other people.

## The Bad

I have quite a few bad things to say about the reMarkable though; it is certainly far from perfect.

Lets start with the issue I probably noticed first:
The version of software I got was already at least `6` months out-of-date.
This is of course easy to fix by myself, nevertheless manufacturers should ensure that the device they sell should be up-to-date from the factory.
Furthermore, during my first update of the software, for some reason unknown to me, all documents I deleted on the device  (i.e. moved to the trashcan) came back.
But as this did not happen with any subsequent software updates, this is not a massive issue.

While I previously noted that the hardware of the device was pretty good, I left out a few issues.
Firstly, the device only has 6GB of storage capacity.
While documents do not take up too much space in general, storage is pretty cheap so why not just throw in 64GB or more?
The display is also not too high-resolution.
I do write pretty small, such that if I look somewhat closely I can easily note how my writing is pixelated.
I guess this is another limitation of the e-ink technology, and I would also not consider it to be a massive problem as my writing is still legible (well, as legible as it is on paper).

Probably one of the worst issues of the device though is that it sometimes (extremely rarely, probably at most `1%` of the time) it has a noticeable delay (probably `100+ms`, but did not measure) when writing.
I guess this is related to updating the screen, but it is pretty infuriating when it happens as I then stop writing in the middle of the word, the text appears while I already took my pen away from the display and I need to rewrite the word after deleting it.
Another very bad issue is that the display seems to be badly calibrated in some areas (for me it is in the top-left corner and also on the right edge).
This means that the writing happens `1-2mm` from where I actually set down my pen.
This can of course also be very infuriating.

## Wishlist

There are a few things I would love to have on my reMarkable, which mostly boil down to one use-case.
During my study, many lectures publish a script of the lecture material.
This script is regularly updated depending on how far we progressed in the lecture.
I would therefore want a way to transfer my remarks from one version of the script to a newer version.
As an alternative, one could also think of moving PDF pages with annotations between documents instead.
While such a feature probably does not make sense for most PDF documents (e.g. transferring notes from one lecture to the next), I would really love to have it for my use case.
Currently, taking notes on scripts for such kinds of lectures is pretty much infeasible.

Further minor things in my wishlist include snapping of lines to the background grid, if such a grid is set, and the improvements suggested regarding the USB API in the "meh"-section.
And please OCI on-device.
Please.

## Conclusion

In general, I really like the reMarkable 2.
But not for its hardware, or software, but instead as it is an easy way to take hand-written notes which I have always with me.
While I tried using paper for that before (mainly for study-related things, but also for other notes), I did not really stick with that for general things.

I furthermore find it bad to see the focus reMarkable seems to put on its cloud service, which I do not want to use.
While I recently discovered Supernote as a good alternative for me, I do not want to switch away any time soon.
