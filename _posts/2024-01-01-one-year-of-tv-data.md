---
layout: post
title: One Year of TV Data
tags: [ tv ]
---

Last year I decided to scrape data on what is shown on TV.
Don't ask me why I did that, it sounded fun (and also was fun).

Before I start, I think I need to clarify at least two more things that may be important.
Firstly, I will not share where I have the data from, but it is from a popular website providing TV programs for Germany.
Secondly, I will not share the data I have gathered, and I will also round the results I got significantly.
This ensures that I don't have into any legal issues (even though to my knowledge scraping is legal where I am, I don't know if sharing my exact results is).

Note that the data was scraped daily and only once per day, which means that the actual program sent may deviate from the scraped data due to changes in the program between scraping-time and program-time.
Also note that the results will obviously mostly be German, I will not translate most of the results.
Anyway, now that I have the data of the last year, lets analyze it.

## Overview

One record of scraping is compromised into seven different fields.
This contains the channel the data is from, the date when the record starts, the duration the program will run for, the title of the program and finally the genre, division and rating as specified by the website I scraped.

In total, I scraped about 2.5 million records for about 200 channels.
This amounts to about 7000 records scraped each day.

One can watch about 100 million minutes of content, with the about 200 channels this nicely corresponds to about one year per channel.
Not sure where the remaining few days got lost, but maybe some channel turned off during the scraping.

## Most Played

Across all channels, the following titles were played the most:

| Title                   | Played |
|-------------------------|--------|
| Teleshopping            | 23.500 |
| The News                | 19.000 |
| Le journal              | 16.500 |
| SpongeBob Schwammkopf   | 14.000 | 
| Infomercial             | 13.500 |
| BBC News                | 12.000 |
| Fußball: Premier League | 11.000 |
| News                    | 10.500 |
| Tagesschau              | 10.000 |
| Teen Titans Go!         |  9.000 |

One can note that aggregated news are by far the most entries, most of the news entries are even global and not local.
Teleshopping also is sent out extremely often - not sure who watches those.
There are also a few kids-shows like "SpongeBob Schwammkopf" in there. 
The number of times a title is played is does not correspond to the duration they were played though, for example news often are shorter.
I therefore also computed the total duration for each title.

| Title                   | Total Duration |
|-------------------------|----------------|
| Teleshopping            | 800 days       |
| Infomercial             | 500 days       |
| Fußball: Premier League | 400 days       |
| BBC News                | 350 days       |
| Tennis: ATP World Tour  | 300 days       |
| Sky Sport News HD       | 300 days       |
| eSports: ESL Event      | 250 days       |
| SpongeBob Schwammkopf   | 250 days       |
| Closedown               | 200 days       |

One can watch more than 2 years of Teleshopping per year.
Still don't know who is watching this.
Furthermore, sports also seem to be very high with football, tennis and E-sports (and sport news) in this list.
One can also watch over 200 days of SpongeBob per year.

## Only for Main Channels

The website I am scraping also has a notion of "main channels", which are about 20 of their channels which are popular.
I decided to do the same analysis as above, but only for those main channels:

| Title                                                | Played |
|------------------------------------------------------|--------|
| Tagesschau                                           | 3.500  |
| South Park                                           | 3.500  |
| Modern Family                                        | 2.500  |
| Bob's Burgers                                        | 2.500  |
| Infomercial                                          | 2.500  |
| American Dad                                         | 2.500  |
| Medical Detectives – Geheimnisse der Gerichtsmedizin | 2.500  |
| The Big Bang Theory                                  | 2.000  |
| Futurama                                             | 2.000  |
| Malcolm mittendrin                                   | 2.000  |

| Title                                                | Total Duration |
|------------------------------------------------------|----------------|
| Infomercial                                          | 150 days       |
| Medical Detectives – Geheimnisse der Gerichtsmedizin |  75 days       |
| Der Trödeltrupp – Das Geld liegt im Keller           |  75 days       |
| Das Familiengericht                                  |  50 days       |
| South Park                                           |  50 days       |
| Das Jugendgericht                                    |  50 days       |
| CSI: Miami                                           |  50 days       |
| Der Blaulicht Report                                 |  50 days       |
| Das Strafgericht                                     |  50 days       |
| Modern Family                                        |  50 days       |

One sees a significant reduction in Teleshopping, mainly in favor of series of different kinds.
It still includes tons of infomercials though.

## Popular Movies

It often seems like certain movies which are worth watching run weekly.
This is for example the case for "James Bond" and also "Harry Potter".
I therefore want to answer the question how often I can watch those per year.

| Title        | Played | Played in Main Channels | "Meta-Movies"       |
|--------------|--------|-------------------------|---------------------|
| James Bond   | 260    | 80                      | 2                   |
| Harry Potter | 210    | 45                      | 35                  |

One can watch both James Bond and Harry Potter pretty much daily (assuming the days where they are played don't overlap, which is probably a bad assumption).
Even with the main channels, one can watch James Bond more than weekly, and Harry Potter almost weekly.
The large difference between the number of plays in the main channels probably stems from the fact that James Bond has way more different movies compared to Harry Potter.
But looking at the meta-movies I have found - movies that only talk about the movie series and are not actually part of the series - Harry Potter has way more of those.
This mostly stems from 20th anniversary highlights, where one entry was played almost 20 times.
I don't really understand this, as to my knowledge Harry Potter had its 20th anniversary 2021 and not 2023.

## Ratings

The website I scrape also provides rating data for some of the movies, with ratings from 1 to 3, with 1 the best and 3 the worst rating.
The following distribution of the rating was noticed:

|    Rating | Number of Entries |
|-----------|-------------------|
| No Rating |         2.500.000 |
|         1 |           100.000 |
|         2 |            40.000 |
|         3 |            10.000 |

As expected, the majority of movies are unrated.
From the rated movies, the average rating is about 1.4, which is very much biased towards good ratings.
This is also expected, they don't bother with rating bad movies in the first place.

The genres which were also very different in the amount of rating per genre, with the most rated genres being thrillers, dramas or comedies.

## Other Fun Observations

There were 14 entries (deduplicated by title) I scraped which were only for people above 99 years old, and even two which were aimed at "children older than 99 years".
There was even one "entertainment for 2013 or older"; not sure anyone is allowed to watch this one.

The longest entry title was almost 150 characters long, it was some religious mass.
The shortest ones were the single-character letters "X" (which seems to be some horror movie) and "c" (which is either a drama, a cooking show or a documentation).

The longest running entry by duration were "intermissions" for an entire day, as well as some nature reports.
There were also numerous 0-minute entries, not sure if those are errors of my script or they really happened.

## Conclusion

I am certainly no expert in data analysis, I therefore also cannot provide any real results from my scraping.
Nevertheless, doing the scraping and analysis was pretty fun.
Maybe I will do another post if I can think of any more things to analyze (or you ask me to analyze something), and maybe I will even continue the scraping and analysis for the upcoming year.
