---
layout: post
title: Introducing μnit
# project arc munit university programming nushell nix tuxedo tv rant review remarkable-2 book game movie 
tags: [ project munit ]
---

Oops, I did it again.
I started another sideproject (to be honest, I already started that at the beginning of this year, but only came around to publishing it right now).
This project is called munit (with the greek letter `mu` if one wants to be fancy, but I really don't want to copy-paste that letter in this article dozens of times).
Munit is an extremely simple calculator, which has the "superpower" to work with arbitrary units and convert between those.

You can find the repository [on GitLab](https://gitlab.com/Schmiddiii/munit).

Note that one probably should not use it for anything useful, this project was just for me experimenting in creating a project-language in the future (I don't have concrete plans for that yet though).
In fact, I actually don't plan to evolve this project much further.
It already does everything I planned to do with it

## Why

As mentioned, this project was mostly for learning how to create programming languages.
Nevertheless, this project will hopefully also be useful for me personally, as an extremely simple CLI calculator and as an alternative to nushell in this minor regard (see also my [blog post where I mentioned I wanted such a calculator](https://schmidhuberj.de/2023/11/01/why-i-stopped-using-nushell.html)).

## Example

The following is an example of using munit. I just copied it from the repositories README.

```
>> 1 + 2
3
>> 1s + 2s
3s
>> :convert 1s = 1000ms
1s = 1000ms
>> 1s + 2000ms
3s
>> :like ms
Like ms
>> 1s + 2000ms
3000ms
>> :convert 1i^2 = 0 - 1
1i^2 = -1
>> (1 + 2i) * (3 + 4i)
-5 + 10i
>> :exit
```

## Conclusion

I will keep this post pretty short, as this is already everything I have to say in this regard.
It was a fun project when I worked on it, but I don't plan to develop it much further.
