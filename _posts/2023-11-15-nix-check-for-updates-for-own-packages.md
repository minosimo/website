---
layout: post
title: "Nix: Check for Updates for Own Packages"
tags: [ nix ]
---

Recently, I played around some more with Nix (with Flakes enabled), including refactoring my configurations and also adding a package for something not yet packaged in nixpkgs. While I was satisfied with the result, I was sure I will forget updating my own package most of the time (as I will have to manually update the git revision/version for new updates, due to reproducibility reasons). I therefore needed a simple way from which I can tell which of my (in total two) custom-packaged programs are out-of-date and need updates.

Note that the way I am doing things is probably not the most optimal; I am sure a proper nixpkgs-maintainer has some improved scripts doing the same thing but better (at least I hope so, but sometimes looking at the package versions I am not sure if there is such a script).

So, let's get started with that problem. The first thing I though of when trying to solve that was to just inject the check the check during the package build, doing everything in pure nix. But this of course does not work, due to reproducibility reasons. Therefore, the check needs to happen outside of nix. I therefore thought about some script that queries fr every package the packaged version (using `nix expr`), queries the current version (using a mix of `curl` and `jq`) and compares them. While this would work, it would not be economic, it requires editing the script for every package I had and also implement different queries for each package (and I would somehow need to figure out how to query things looking at the keys of the package). Thus, I made nix generate me that script.

The basic idea of this is that each nix package itself knows where to query the current version from and what the packaged version is. Using this information, each package has a "subpackage" that builds a bash-script which could be executed to find out if there is any version mismatch. After some testing and refactoring, I came out with a result which I think is pretty ergonomic and easy-to-use.

The core of this check is a deviation, which takes the name of the package to check, a bash-script on how this check happens and what the expected version is. This generates a script that would run this check and compare it with the packaged version; if there is a difference it would print it out.

```nix
# File: check-latest-version.nix
default = { name, check, version }:
  let
    script = ''
      #!/usr/bin/env bash
      version=\$(${check})
      if [ "\$version" != "${version}" ]; then
        echo "${name}: New Version: \$version"
        exit 1
      fi
    '';
  in
  pkgs.stdenv.mkDerivation {
    name = "${name}-check-latest-version";

    phases = [ "installPhase" ];

    installPhase = ''
      mkdir -p $out/bin
      echo -n "${script}" > $out/bin/check-latest-version
      chmod +x $out/bin/check-latest-version
    '';
  };
```

To make it more ergonomic to use, I also made specializations for usage with GitHub and crates.io:

```nix
# File: check-latest-version.nix
github = { name, owner, rev }:
  let
    check = "${pkgs.curl}/bin/curl 'https://api.github.com/repos/${owner}/${name}/commits/main'  2> /dev/null | ${pkgs.jq}/bin/jq -r .sha";
  in
  default { inherit name check; version = rev; };

crates-io = { name, version }:
  let
    check = "${pkgs.curl}/bin/curl 'https://crates.io/api/v1/crates/${name}/versions'  2> /dev/null | ${pkgs.jq}/bin/jq -r '.versions | .[0] | .num'";
  in
  default { inherit name check version; };
```

Inside a package, I can now use these derivations (which are under the namespace `check-latest-version`; I know, a terrible name) to specify how the version check happens. An example usage in [Telekasten.nvim](https://github.com/renerocksai/telekasten.nvim):

```nix
# File: telekasten-nvim.nix
{ pkgs, check-latest-version }:
let
  name = "telekasten.nvim";
  owner = "renerocksai";
  rev = "3fd50c62c1ed2f973ce8639c1218445e4e91a962";
  sha256 = "sha256-lLy83LiGhyAdVV82YSJUfWyLqxK3ghFJ/FWNxBFl7mQ=";
in
pkgs.vimUtils.buildVimPlugin {
  inherit name;

  src = pkgs.fetchFromGitHub {
    inherit owner rev sha256;
    repo = name;
  };

  check-latest-version = check-latest-version.github {
    inherit name owner rev;
  };
}
```

I can therefore build and execute `packages.telekasten-nvim.check-latest-version` to check if I am currently using the latest available version. In fact, I can do that for all of my (two) packaged packages I have.

But manually building and executing this does not sound nice to me. I want one single command which checks all the versions. Another instance where nix comes to the rescue, I can just build a derivation which creates a script running all other derivations (sounds complicated, the code also is complicated, but the idea is pretty simple):

```nix
# File: check-latest-version.nix
forAll = input:
  let
    lib = pkgs.lib;

    torun = builtins.filter (c: builtins.hasAttr "check-latest-version" c) (builtins.attrValues input);
    toexecute = builtins.map (c: "${c.check-latest-version}/bin/check-latest-version") torun;
    script = builtins.concatStringsSep "\n" ([ "#!/usr/bin/env sh" ] ++ toexecute);
  in
  pkgs.stdenv.mkDerivation {
    name = "check-latest-version-for-all";

    phases = [ "installPhase" ];

    installPhase = ''
      mkdir -p $out/bin
      echo -n "${script}" > $out/bin/check-latest-version-for-all
      chmod +x $out/bin/check-latest-version-for-all
    '';
  };
```

This script first filters all packages which have an "check-latest-version" subpackage, and then builds and installs a script running each of those derivations (to be honest, I don't know what would happen if I run the resulting derivation but no of the subpackages has been built, or if such a thing is even possible).

But I also don't want to specify to the derivation which packages it should check (otherwise, I will just forget giving it that package, which therefore will not receive any updates). Luckily, I already have a file containing all the custom packages I have, I can just give this as an input to the `forAll`. This required a few changes to that file I had previously, which now looks like this:

```nix
{ pkgs }:
let
  check-latest-version = pkgs.callPackage ./check-latest-version.nix { };

  callPackage = pkg: over: pkgs.callPackage pkg (over // { inherit check-latest-version; });

  packages = {
    telekasten-nvim = callPackage ./telekasten-nvim.nix { };
    cargo-upgrades = callPackage ./cargo-upgrades.nix { };
  };
in
packages // {
  check-all-versions = check-latest-version.forAll packages;
}
```

I first load the `check-latest-version.nix` which contains all required checks, wrap a new `callPackage` which includes this `check-latest-version`, specify the packages I want and finally add the `check-all-versions`-package, which checks all versions for all packages (excluding itself).

I therefore now can use `nix run .#check-all-versions` (with some more minor changes in the `flake.nix`) to check that all my custom packages are up-to-date. As already mentioned, I am sure there are better ways to do that out there, but I have not yet found any. I am also sure that the code I wrote is pretty terrible (this is the first non-trivial nix package I wrote). But it seems to work.

---

Below you can find all the syntactically correct code I used in this post:

```nix
# File: default.nix
{ pkgs }:
let
  check-latest-version = pkgs.callPackage ./check-latest-version.nix { };

  callPackage = pkg: over: pkgs.callPackage pkg (over // { inherit check-latest-version; });

  packages = {
    telekasten-nvim = callPackage ./telekasten-nvim.nix { };
    cargo-upgrades = callPackage ./cargo-upgrades.nix { };
  };
in
packages // {
  check-all-versions = check-latest-version.forAll packages;
}
```

```nix
# File: check-latest-version.nix
{ pkgs }:
rec {
  default = { name, check, version }:
    let
      script = ''
        #!/usr/bin/env bash
        version=\$(${check})
        if [ "\$version" != "${version}" ]; then
          echo "${name}: New Version: \$version"
          exit 1
        fi
      '';
    in
    pkgs.stdenv.mkDerivation {
      name = "${name}-check-latest-version";

      phases = [ "installPhase" ];

      installPhase = ''
        mkdir -p $out/bin
        echo -n "${script}" > $out/bin/check-latest-version
        chmod +x $out/bin/check-latest-version
      '';
    };

  forAll = input:
    let
      lib = pkgs.lib;

      torun = builtins.filter (c: builtins.hasAttr "check-latest-version" c) (builtins.attrValues input);
      toexecute = builtins.map (c: "${c.check-latest-version}/bin/check-latest-version") torun;
      script = builtins.concatStringsSep "\n" ([ "#!/usr/bin/env sh" ] ++ toexecute);
    in
    pkgs.stdenv.mkDerivation {
      name = "check-latest-version-for-all";

      phases = [ "installPhase" ];

      installPhase = ''
        mkdir -p $out/bin
        echo -n "${script}" > $out/bin/check-latest-version-for-all
        chmod +x $out/bin/check-latest-version-for-all
      '';
    };

  github = { name, owner, rev }:
    let
      check = "${pkgs.curl}/bin/curl 'https://api.github.com/repos/${owner}/${name}/commits/main'  2> /dev/null | ${pkgs.jq}/bin/jq -r .sha";
    in
    default { inherit name check; version = rev; };

  crates-io = { name, version }:
    let
      check = "${pkgs.curl}/bin/curl 'https://crates.io/api/v1/crates/${name}/versions'  2> /dev/null | ${pkgs.jq}/bin/jq -r '.versions | .[0] | .num'";
    in
    default { inherit name check version; };
}
```

```nix
# File: telekasten-nvim.nix
{ pkgs, check-latest-version }:
let
  name = "telekasten.nvim";
  owner = "renerocksai";
  rev = "3fd50c62c1ed2f973ce8639c1218445e4e91a962";
  sha256 = "sha256-lLy83LiGhyAdVV82YSJUfWyLqxK3ghFJ/FWNxBFl7mQ=";
in
pkgs.vimUtils.buildVimPlugin {
  inherit name;

  src = pkgs.fetchFromGitHub {
    inherit owner rev sha256;
    repo = name;
  };

  check-latest-version = check-latest-version.github {
    inherit name owner rev;
  };
}
```

```nix
# File: cargo-upgrades.nix
{ pkgs, check-latest-version }:
let
  lib = pkgs.lib;
  rustPlatform = pkgs.rustPlatform;
  fetchCrate = pkgs.fetchCrate;

  pname = "cargo-upgrades";
  version = "2.0.0";
in
rustPlatform.buildRustPackage {
  inherit pname version;

  src = fetchCrate {
    inherit pname version;
    sha256 = "sha256-5fSbVVxgXY3H+2jQgoSC952J+5zO1vuHJ+9pm0c4jc8=";
  };

  cargoSha256 = "sha256-8/Ojw3vmLLrYppB8oNRC0vbYmwco5668YpKbwAgivoU=";

  check-latest-version = check-latest-version.crates-io {
    inherit version;
    name = pname;
  };
}
```
