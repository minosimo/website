---
layout: post
title: Introducing Zettler
# project arc munit zettler university programming nushell nix tuxedo tv rant review remarkable-2 book game movie 
tags: [ project zettler ]
---

Oops, I did it again.
I started another sideproject.
Zettler is a LSP server for managing a zettelkasten (or similar personal knowledge bases).

The repository of Zettler can be found [on GitLab](https://gitlab.com/Schmiddiii/zettler/-/tree/master?ref_type=heads).

## Why

For about two years now, I have been keeping a zettelkasten-like folder for keeping notes (I still don't know if I am actually doing it the "right" way, but as long as it works for me, I don't care).
During that entire timeframe have I been using [telekasten.nvim](https://github.com/renerocksai/telekasten.nvim) to manage that zettelkasten.
A few months ago, I have started using [Helix](https://helix-editor.com/) as an editor, which slowly got my preferred editor of choice for pretty much everything (this previously was neovim).
Now, I pretty much only use neovim for my zettelkasten.
I wanted to change that, but found the other options to not match my workflow that I am used to (see also [similar projects](https://gitlab.com/Schmiddiii/zettler/-/blob/master/doc/Similar%20Projects.md?ref_type=heads) doc in Zettler).

This is why I started writing Zettler, for managing my zettelkasten with Helix.
Being a project for me personally, this project also does not have the goal to be very configurable; nevertheless it should be easy to modify the code to make it work for zettelkasten of different people.

## Features

Zettler has all the basic features I expect from such an utility, packaged inside an LSP server:

- Creating new notes (via "workspace commands").
- Listing notes (via "workspace symbols").
- Jumping between notes (via "go to definition").
- Inserting links to other notes (via "completion").
- Peeking at a note (via "hover").
- Renaming note (via the data gathered in yaml frontmatter).

I am especially proud of renaming notes, this is just as easy as changing the title in the frontmatter.
One feature that is still lacking though, and which I have not yet found a good way to integrate, is specifying which template to create a document from.
Something like this is already implemented, via different workspace commands, but there is no nice UI to do that for now.

## Conclusion

Another new sideproject.
Let's see where this one goes.
I feel like this will be a pretty useful project for me personally, I can't yet estimate if it will have further reach though.
