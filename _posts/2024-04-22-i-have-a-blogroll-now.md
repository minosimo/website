---
layout: post
title: I have a blogroll now
# meta project arc munit zettler university programming nushell nix tuxedo tv rant review remarkable-2 book game movie 
tags: [ meta ]
---

This is just an extremely short post to inform you that I have a blogroll about some of my favourite blogs now.
You can find it [here](http://localhost:4000/blogroll) (or in the header bar of the website).
