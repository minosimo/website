---
layout: post
title: On the Repairability of Tuxedo Laptops
tags: [ tuxedo ]
---

I have had the [Tuxedo InfinityBook Pro 14 v5](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/10-14-inch/TUXEDO-InfinityBook-Pro-14-v5.tuxedo) for about 4 years now and during that timeframe made some upgrades to the machine I originally got (which was pretty much the weakest version they sold back then). In total, I installed a new HDD, swapped out the SSD for a bigger one, increased my memory to 16GB and swapped out the battery. As I now already have some experience with upgrading this laptop, I want to share my findings in regards to repairability - maybe it will be useful for others (even though the laptop is not sold anymore).

Note that I am in no way experienced in doing this, but I would guess so are most other users of such laptops.

# First Opening

Due to an issue I had with the WiFi card, I had to open the laptop soon after I got it, and this already made some problems. While unscrewing most of the screws was pretty easy, there was at least one that did not want to be removed. We had to apply significant force to remove it, something one does certainly not want to do on a new laptop. During that, I also made a scratch into the back of my laptop, which is not nice to have. After getting the final screw out, the back place could still not be removed. It took us quite a while to figure out that we had to remove the keyboard (which was also not too easy) and there were some more screws there. I probably should have known that, maybe I should have watched their video of disassembly of the laptop. Nevertheless, not too nice, but once you know that, there is now no problem with opening up the laptop.

Sadly, I must have broken one of the connectors between the keyboard and the motherboard while opening the laptop, which is why my backlight does not work. But as I don't use it anyway, this is not a huge problem.

# Installing the HDD

Installing the hard drive was no problem at all, as I already knew how to open up the laptop. But I seem to have picked a terrible HDD to install, as this is so much slower than my SSD and it feels like it slowed down my laptop a lot (I also made the mistake to move my home partition onto it). This was my fault though.

# Upgrading the SSD and RAM

This was no problem at all. Everything went smooth.

# Replacing the Battery

As my battery was already getting somewhat weak (after four years of daily usage), I decided to replace it recently. So I bought myself a replacement battery from Tuxedo Computers (thanks for providing this by the way, probably would have never found a battery that fits) and replaced it. Opening it again was no issue, but the screws holding in the battery were screwed in extremely hard. I had to resort to my grandpa for help, and we required significant force again to get two of them out. One posed even harder than the other (I would guess as we already tried for quite some time to get it out, wore off the screw and therefore could not get anymore grip). But we finally managed to remove both of them.

But looking closer, it seems like the screwdriver must have slipped once and scratched the motherboard. And even worse, I think it may have removed one transistor during its rampage. Putting everything together as it was, it looks like the laptop still works correctly, so I guess this specific must not have been vital.

# Conclusion

It is nice that Tuxedo Computers enables reparation of its internal components, which is why I can still run my laptop right now (my storage and memory requirements have grown significantly since I bought it originally). Nevertheless, reparation is not as simple as one would hope. So, thank you Tuxedo Computers for making your products reparable, but could you please not stop tightening the screws that much?
