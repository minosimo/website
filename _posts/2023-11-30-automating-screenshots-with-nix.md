---
layout: post
title: Automating Screenshots with Nix
tags: [ nix ]
---

I am the developer of a few GTK application ([more information](https://gitlab.com/schmiddi-on-mobile)) and every good GUI application needs a screenshot to advertise the capabilities of the application. For that purpose, I (or another person) usually manually had to take screenshots of my application. The problem with that was that I am not using GNOME (shame on me), thus my screenshot looked a little bit off (to be honest, I never noticed it, but was told so by others).

After a little bit of discussion, that I could either automate screenshot-taking in CI, and use a virtual machine - this would not be nice user-wise, it would always take quite a while to start, I would need to compile my software and then transfer the screenshot. I thus automated taking screenshots using nix, (ab)using their testing-capabilities. And even though the path to the (hopefully final) iteration was pretty hard, it was also really fun to delve more into nix and also feels cool just running a single command and it (headlessly) creates a screenshot of the current state of the application.

## Initial Attempt

My initial attempt was actually without using a VM, just with `xvfb` and `xdotool`. I even successfully created screenshots using that, but I never managed to figure out how to set up the correct font, or how to ensure internet access (which I will not need for this specific application, but maybe for others).

I will nevertheless share the code for this failed attempt (note that I am screenshotting the derivation `packages.screenshot` (which is a special derivation just for screenshots) with window name `flare`):

```nix
pkgs.stdenv.mkDerivation {
  name = "${name}-screenshot";
  phases = [ "buildPhase" "installPhase" ];
  nativeBuildInputs = with pkgs; [ xvfb-run xdotool packages.screenshot imagemagick gtk4 gsettings-desktop-schemas gnome.adwaita-icon-theme cantarell-fonts ];
  buildPhase = ''
    export GSETTINGS_SCHEMA_DIR=${pkgs.gtk4}/share/gsettings-schemas/${pkgs.gtk4.name}/glib-2.0/schemas/:${pkgsgnome.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgsgnome.gsettings-desktop-schemas.name}/glib-2.0/schemas/:./build/data/
    export DISPLAY=:99
    export HOME=.
    xvfb-run --server-args="-screen 0 1400x1000x24" flare &
    gsettings set org.gnome.desktop.interface font-name 'Cantarell 12'
    sleep 5
    xdotool search --onlyvisible --name flare windowsize 1400 1000
    sleep 1
    xdotool key Tab
    sleep 0.25
    xdotool key Tab
    sleep 0.25
    xdotool key Tab
    sleep 0.25
    xdotool key Return
    sleep 1
    import -window root screenshot.png
  '';
  installPhase = ''
    mkdir -p $out
    cp screenshot.png $out/
  '';
};
```

## (Ab)Using Nix Tests

Nix has a handy feature for testing NixOS. Disregarding what I will use this for, this is a great idea to directly use Nix to test the packaged results behave correctly. But I will abuse this feature to create a screenshot of my application.

Nix tests specify a NixOS configuration, which will be run in a VM, in addition to a python script, which will be used for controlling the virtual machine. To take a screenshot of the application, we will therefore specify a NixOS configuration running GNOME which will start the application to screenshot, press a key-combination that takes a screenshot of the focused application in GNOME and save that screenshot in the results. While the concept was pretty simple, tons of issues regarding Nix and Qemu popped up (or probably mainly my missing knowledge). The main issue was getting a good resolution of the virtual display. I tried many different things, but finally managed to set a specific Qemu option to do so. 

The final derivation I created is then the following:

```nix
let
  nixos-lib = import (nixpkgs + "/nixos/lib") { };
in
nixos-lib.runTest {
  name = "screenshot";
  hostPkgs = pkgs;
  imports = [
    {
      nodes = {
        machine = { pkgs, ... }: {
          boot.loader.systemd-boot.enable = true;
          boot.loader.efi.canTouchEfiVariables = true;

          services.xserver.enable = true;
          services.xserver.displayManager.gdm.enable = true;
          services.xserver.desktopManager.gnome.enable = true;
          services.xserver.displayManager.autoLogin.enable = true;
          services.xserver.displayManager.autoLogin.user = "alice";

          virtualisation.qemu.options = [ "-device VGA,edid=on,xres=1920,yres=1080" ];

          users.users.alice = {
            isNormalUser = true;
            extraGroups = [ "wheel" ];
            uid = 1000;
          };

          system.stateVersion = "22.05";

          environment.systemPackages = [
            self.packages.${system}.flare-screenshot
          ];

          systemd.user.services = {
            "org.gnome.Shell@wayland" = {
              serviceConfig = {
                ExecStart = [
                  ""
                  "${pkgs.gnome.gnome-shell}/bin/gnome-shell"
                ];
              };
            };
          };
        };
      };

      testScript = { nodes, ... }:
        let
          user = nodes.machine.users.users.alice;
          username = user.name;
          uid = toString user.uid;

          bus = "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/${uid}/bus";
          su = command: "su - ${user.name} -c '${command}'";

          sleep = {
            short = toString 1;
            long = toString 10;
          };

          lib = pkgs.lib;
          keysequence = (lib.lists.replicate 5 "tab") ++ [ "ret" ];
          executekeys = lib.concatStringsSep "\nmachine.sleep(${sleep.short})\n" (map (x: "machine.send_key(\"${x}\")") keysequence);

          # Start Flare
          launchFlare = su "${bus} gapplication launch de.schmidhuberj.Flare";
        in
        ''
          machine.sleep(${sleep.long})
          machine.send_key("esc")
          machine.sleep(${sleep.short})
          machine.succeed(
              "${launchFlare}"
          )
          machine.sleep(${sleep.long})
          machine.send_key("esc")
          machine.sleep(${sleep.short})
          ${executekeys}
          machine.sleep(${sleep.long})
          machine.send_key("alt-print")
          machine.sleep(${sleep.short})
          machine.execute("mv /home/${username}/Pictures/Screenshots/* screenshot.png")
          machine.sleep(${sleep.short})
          machine.copy_from_vm("screenshot.png", ".")
        '';
    }
  ];
};
```

## Conclusion

This derivation may not look like much, but it took me about two weeks to get to a point where I was really happy with the results. At least one week was spent on debugging why the resolution is not as I want it to be. (Note that I also had other things to do in those two weeks, not everything was spent on this single derivation). One can therefore conclude that Nix is pretty nice to automate such things - not even 100 lines of code for starting up a VM with my custom application in it, starting the application and taking the screenshot sounds impressive. Nevertheless, it is certainly far from easy to do such things (now that I know how to do it, it probably is pretty easy) and it takes quite a while to debug weird issues.

## References

The following were the main sources used to create this derivation:

- <https://github.com/NixOS/nixpkgs/blob/master/nixos/tests/gnome.nix>
- <https://nixos.org/manual/nixos/stable/#sec-nixos-tests>
- <https://wiki.archlinux.org/title/QEMU>

If you want to see the full code change, check out [this MR](https://gitlab.com/schmiddi-on-mobile/flare/-/merge_requests/102).
