---
layout: post
title: The TIOBE Index has Gone Crazy
tags: [ rant, programming ]
---

I recently got told something very disturbing:
[Scratch](https://scratch.mit.edu/) is now in the top 10 of the TIOBE index, and the second most growing language.
I have not yet seen anyone use Scratch to do any serious project.
Yes, there are people writing [Linux kernel modules](https://github.com/scratchnative/scratch-linux-modules) or even an [Operating system](https://github.com/scratchnative/scratchOS) in Scratch, but those are just fun sideprojects (at least I hope so).

Anyway, there is probably already enough ranting about how terrible the TIOBE index actually is out there, e.g. [in this blog post from 2009](https://blog.timbunce.org/2009/05/17/tiobe-index-is-being-gamed/) or also [some hacker news discussions](https://news.ycombinator.com/item?id=19767454).
Instead, why not make my own rating of programming languages this year.
After all, I'm something of a programmer myself (and I used a few (52) programming languages last year).
Note that this list does (mostly) not contain all-time favorites, but programming languages I have tried last year and found nice.
Most of those programming languages I will likely not use for quite some time.

## Schmiddis Programming Language List

### [Rust](https://www.rust-lang.org/)

This one is obvious, after all it is my main programming language for the last few years.

### [Julia](https://julialang.org/)

I really enjoyed this language, also based on how easy it is to program for the GPU.
Nevertheless, it sometimes has some weird error messages when using the GPU (for example when using a variable that is global and not local), and it also being slow when initializing packages

### [Nim](https://nim-lang.org/)

An extremely simple programming language, with a good standard library.
Feel pretty nice to use, with no surprises attached.

### [Elm](https://elm-lang.org/)

An interesting concept to do GUI.
The main reason I add it here though is the debugger.
Going back in time sounds like a great idea, even though I did not end up using it too much in my project as there were no issues during development of my project.

### [Futhark](https://futhark-lang.org/)

A programming language you probably never heard of before, because it seems to be mostly a research project.
But it is a really interesting programming language if I may say so.
It is basically programming for the GPU in simple.
And integrating it into Python is trivial.

## Conclusion

TIOBE index is still a terrible source for programming language "popularity".
Instead of relying on them to rank programming languages, why not make a personal ranking?
And if you decide to make a personal ranking yourself, please share it with me.
