---
layout: blogroll
title: Blogroll
---

This page lists a few blogs that I greatly enjoy, along with an extremely short description about what I liked about their posts.
Note that this is only a subset of all blogs I subscribe to.
Furthermore, some of them may not be active any more, but nevertheless have some good reading material one can read in ones free time.

This page is ordered by when I subscribed to the blog, with new entries appearing at the bottom.
I may update this page from time to time, but I may also not do that.
Time shall tell.

- [lisyarus blog](https://lisyarus.github.io/blog/):
  A nice blog which does some very nice deep-dives into some topics.

- [Xe Iaso](https://xeiaso.net/):
  A (probably) well-known blog talking about a lot of different, but nevertheless interesting things.

- [TheEvilSkeleton](https://theevilskeleton.gitlab.io/feed.xml):
  Mostly posts related to Flatpak or GNOME, which I am also passionate about, but also other posts that are enjoyable.

- [Jim Nielsen’s Blog](https://blog.jim-nielsen.com/):
  Mostly short posts about mostly web-related technology.
  Even though I am not a web-developer, I still enjoy the posts.

- [matklad](https://matklad.github.io/):
  A blog going extremely in-depth about rust topics.
  Sometimes too deep.

- [Orhun's Blog](https://blog.orhun.dev/):
  A blog mostly about a Rust CLI library, which I do not use but where I enjoy reading the blogs from.

- [The Coded Message](https://www.thecodedmessage.com/):
  A blog about many programming-related things.

- [Lazybear](https://lazybear.io/):
  Many posts I enjoy, with loosely programming-related topics.

- [The Daily WTF](https://thedailywtf.com/):
  Some very nice comedy about programming, and that each day (except weekends).

- [Herman's blog](https://herman.bearblog.dev/blog/):
  Lots of blog posts about different things, with a loose focus on technology.

- [Letters To A New Developer](https://letterstoanewdeveloper.com/):
  Even though I am not a new developer any more, there are some great articles in there.
  Sadly stopped posting.

- [Mathew Duggan](https://matduggan.com/): 
  Some pretty good posts about technology.

- [Julia Evans](https://jvns.ca/):
  Probably already pretty well-known to produce good posts, lately mostly about git.

- [mcyoung](https://mcyoung.xyz/):
  Good posts with quite a lot of Rust content.
  Some of that is very complex though.

- [Casey Primozic's Blog](https://cprimozic.net/blog/):
  I especially want to highlight the post ["Wrapping Meshes With Geodesic Walks"](https://cprimozic.net/blog/wrapping-meshes-with-geodesic-walks/), but the other posts are also great.

- [Manuel Moreale](https://manuelmoreale.com/):
  Has a great series "People and Blogs" from which I learned about some good bloggers.
  But his other blogs are also nice.

- [brr](https://brr.fyi/):
  A fascinating blog about living in the Antarctica.
  Sadly not in the Antarctica any more, but such great posts!

- [mbuffett](https://mbuffett.com/):
  Some great technological posts about chess.
  I don't play chess, but the posts are great nevertheless.

- Certificate Monitoring:
  A worthy mention to get updates to server certificates on your RSS feed.
  Just add <https://scrutineer.tech/monitor/cert/your-website.here.rss> (replace `your-website.here` with your website) and you will get those updates in your favourite feed reader.
